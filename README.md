
# P21TranscriptomeExplorer (Prototype)



## A data visualisation tool for biologists

This prototype is part of a project to develop a single platform of bioinformatics tools for biologists of the different teams in the laboratory. 
This is a prototye for a transcriptomic data visualization tool. The development of this tool is based and adapted on RNA-seq data from mice. 
The tool will allow to obtain heatmaps of differentially expressed genes from RNA-seq data according to a list of genes of interest. 
In addition, this tool will provide a visualization of the transcription factors regulating these genes of interest. 


### Dependencies 

* This Prototype uses R (version 3.6.3) and three packages which can be installed with the following R commands : 
```
install.packages("pheatmap")
install.packages("RColorBrewer")
install.packages("DT")
```

Conda Environment dapted to the prototype : 
```{}
git clone https://gitcrcm.marseille.inserm.fr/bidaut/P21TranscriptomeExplorer
conda env create -f Prototype_environment.yml
```


### Input files 

Necessary input files :

- Count file : txt (tab separated) file containing RPKM or TPM gene counts for all experimental condition

- Gene regulation file : txt (tab separated) file TRRUST output for example, it must contain at least 4 columns [Gene, Transcrition Factor, Target, Mode of Regulation] The first column “Gene” is the query gene (submitted to TRRUST)


### Main visualisations

Mainly two types of graphics would be produced, a heatmap according to a gene list and a table containing transcriptional regulation data. 


### Future of the prototype

The prototype should then be transformed into an easily usable Rshiny interface 